<?php
require_once("GameSolver.php");
class GameOutput
{
	function showChallenge($set, $target)
	{
		echo "{" .join(",",$set)."} \n";
		echo "Target: " .$target." \n ";

	}

	function ShowSolution($target, $resulting_string,$closest)
	{
		if($closest==$target)
		{
			$message="Exact";
		}
		else
		{
			$difference=(int)$target-(int)$closest;
			if($difference>0)
			$message="Remaining = +" .$difference;
			else
			$message="Remaining = " .$difference;
			
		}
		echo "Solution: [" ;
		echo $message ."]\n";
		echo $resulting_string."=".$closest."\n \n";

	}

	function showOutput($set,$target,$resulting_string,$closest)
	{
		if($closest==$target)
		{
			$message="Exact";
		}
		else
		{
			$difference=(int)$target-(int)$closest;
			if($difference>0)
			$message="Remaining = +" .$difference;
			else
			$message="Remaining = " .$difference;
			
		}
		echo "{" .join(",",$set)."} \n";
		echo "Target: " .$target." \n";
		echo "Solution: [" ;
		echo $message ."]\n";
		echo $resulting_string."=".$closest."\n";

	}
}


?>
