<!DOCTYPE html>
<html>
<head>
	<title>Blog</title>
	<link href= {{ url('/css/info.css') }} rel="stylesheet">
</head>
<body>
<h1>Blog posts:</h1>
Welcome to my blog
<table id='post-list'>
	<table>
		<tr>
			<th>Title</th>
			<th>Topic</th>
			<th>Author</th>
		</tr>
		@foreach ($posts as $post)
		<tr>
			<td> <a href='/blogWeb/public/postinfo/{{$post->id}} '>{{ $post->title }}</a></td>
			<td> {{ $post->topic }}</td> 
			<td>{{ $post->author }}  </td> 
			<tr>
		@endforeach 
</table>

</body>
</html>