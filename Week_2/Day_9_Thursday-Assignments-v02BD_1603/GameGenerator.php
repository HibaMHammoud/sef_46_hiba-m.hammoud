<?php
class GameGenerator
{
	private $number_of_games;
	//Getter and Setter
	public function __getNumberOfGames()
	{
	//take input from user and make sure it is an integer
	    echo "Please enter the number of games you want to play \n";
	    $number_of_games=readline();
	    return $number_of_games;
	}

	public function __setNumberOfGames($n)
	{
	    this.$number_of_games=$n;
	}
	//getting a random set of integers from big and small set
	public function __getSixRandomNumbers()
	{
	     $six_random_numbers=array();
	    //filling randomly  1---> 4 numbers from small_set the array $six_random_numbers
	     $small_set=array(25,50,75,100);
	     $from_small_set=rand(1,4);
	     $from_big_set=6-$from_small_set;
	     while($from_small_set!=0)
	    {
	     shuffle($small_set);
	     array_push($six_random_numbers, array_shift($small_set));
	     $from_small_set--;
	     }
	   //filling randomly numbers from big_set to  the remaining empty places  of array $six_random_numbers
	    $big_set=array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
	     while($from_big_set!=0)
	     {
	     shuffle($big_set);
	     array_push($six_random_numbers, array_shift($big_set));
	     $from_big_set--;
	     }
	     return $six_random_numbers;
	}
	//getting a random target
	public function __getTarget()
	{
		$my_target=rand(101,999);
		return $my_target;
	}
}


