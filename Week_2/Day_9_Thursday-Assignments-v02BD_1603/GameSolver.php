<?php

require_once("GammeOutput.php");
class GameSolver
{
	private function Sampling($operators,$size,$array=array())
	 {
		if(empty($array))
		{
		    $array=$operators;
		}
		if($size==1)
		{
		    return $array;
		}
		$combinations=array();
		for($i = 0; $i < count($array); $i++)
		{
		    for($j = 0; $j < count($operators); $j++)
		    {
		        $combinations[]=$array[$i].$operators[$j];
		    }
		}
		return self::Sampling($operators,$size-1,$combinations);
	 }

	private function permutations(array $elements)
	{
	    if (count($elements) <= 1)
		 {
			yield $elements;
		 } 
	    else {
			foreach (self::permutations(array_slice($elements, 1)) as $permutation)
			 {
			    foreach (range(0, count($elements) - 1) as $i) 
				{
				yield array_merge(array_slice($permutation, 0, $i),
						[$elements[0]],
				   		 array_slice($permutation, $i)
			       			 );
			    	}
			}
	    	}
	}
	private function isAdmissible($result)
	{
		if(is_int($result) && $result >= 0)
		{
			return true;
		}
		else
			return false;
	}
	private function isDistant($target, $number)
	{
		$difference=$target-$number;
		return abs($difference);
	}

	//The solving function and outputing
	public function reachIt($target, $the_six_numbers)
	{
		$myOutput=new GameOutput();
		$myOutput->showChallenge($the_six_numbers,$target);
		$signs=array('-','+','/','*');
		$signs_combination=self::Sampling($signs,5,array());
		$permutate=self::permutations($the_six_numbers);
		$closest_number='';
		$closest_number_string='';	

		foreach ($permutate as $permutation)
		{
			$numbers_permutation[]= $permutation;
		}
		foreach($numbers_permutation as $num)
		{
			foreach($signs_combination as $sign)
			{
			$eval_op=$num[0];
			$representing_op=$num[0].'';
			$i=1;

			while($i<count($num))
			{

				$representing_op.=$sign[$i-1].''.$num[$i];
				$eval_op=eval("return($representing_op);");

				if(self::isAdmissible($eval_op))
				{
					if(self::isDistant($target,$eval_op)<self::isDistant($target,$closest_number))
					{
					
						$closest_number_string=$representing_op;
						$closest_number=$eval_op;
						if($eval_op==$target)
						{	
						break;
						}
					}
				}
		$i++;
			}
		}

	}
		
		$myOutput->showSolution($target,$closest_number_string,$closest_number);
	}
}
?>

