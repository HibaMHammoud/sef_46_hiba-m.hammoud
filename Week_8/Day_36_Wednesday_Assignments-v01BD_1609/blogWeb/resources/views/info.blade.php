<!DOCTYPE html>
<html>
<head>
	<title>Post info</title>
	<link href={{ url('/css/info.css') }} rel="stylesheet">
</head>
    <body>   
        <table>
            <tr>
                <th>{{ $title }}</th>
            </tr>
            <tr>
                <td>Topic:{{ $topic }}</td>
            </tr>
            <tr>
                <td>Description: {{ $description }}</td>
            </tr>
            <tr>
                <td>Author: {{ $author }}</td>
            </tr>
        </table>
        
        <div class="bar">
            <div class="top-right links">
                <a href="{{ url('/index') }}">Back to blog</a>
                <a href="{{ url('/create') }}">Write your own</a>
            </div>
        </div>   
    </body>
</html>