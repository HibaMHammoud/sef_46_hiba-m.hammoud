@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
      <title>Post</title>
      <style>
        body { 
        padding-bottom: 70px;
        }
        form{
            border-style: solid;
            border-color: black;
            border-style: bold;
            border-radius: 30px;
            padding: 30px; 
            border-width: 15px;
        }
        #top-bar{
         padding-top: 10px;  
        }
        .vertical-center {
            min-height: 100%;
            min-height: 100vh;
            display: flex;
            align-items: center;
        }
      </style>
</head>
    <body>
     <nav class="navbar navbar-default navbar-fixed-top" id='top-bar'>
            <!-- <a href="{{ url('/showProfile') }}">My profile</a>
                <a href="{{ url('/create') }}">Add a post</a>  -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Instagram</a>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Post</a></li>
                  <li><a href="{{ url('/home') }}">Home</a></li>
                  <li><a href="{{ url('/showProfile') }}">My Profile</a></li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron vertical-center">
            <div class="container-fluid">
                <div class="page-header">
                  <h1>Create a  post <small>Instagram</small></h1>
                </div>
                <section>
                    <form  method="POST" role="form"  enctype="multipart/form-data" action="{{ url('/post') }}">
                               {{ csrf_field() }}
                                Select image to upload:
                        <input type="file" name="image" id="image"><br>                        
                        <div class="form-group">
                            <label for="title" >Title:</label>
                                <div  >  
                                    <input class="form-control" type = "text" name = "title" /><br>
                                </div>
                           <label for="text" >description</label>    
                            <div >
                                   <textarea class="form-control" name="description">
                                   </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div >
                               <button type="submit" class="btn btn-primary" >
                                  post
                               </button>
                            </div>
                        </div>
                    </form>   
                </section>   
            </div>
            <nav class="navbar navbar-default navbar-fixed-bottom">
                <div class="container"></div>
            </nav>
        </div>
    </body>
</html>
