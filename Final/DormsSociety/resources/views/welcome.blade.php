@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dorm Society</title>

        <style>
            html, body {
                background-color: #fff;
                color: white;
                font-family: 'Helvetica';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .container{
                height: 400px;
                width: 100%;
            }
            #container1{
                
                background-image: url("../public/images.jpg");
                background-size: 100% 100%;
                background-color: #210B61;
            }
            #container2{
                height:150px;
               
                background-color: black;
            }
            #container3{
                background-color: white; 
            }
            #container4{
                background-color:rgba(171, 25, 19, 0.94); 
            }


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            nav{
                background-color: #000066;
                color: white;
            }

            .navbar-brand{
                color:#000066;
            }
            .search{
             margin-top: 10px;
             font-size: 20px;
             float: left;
            margin-left: 20px;
            }
        </style>
        
    </head>
    <body>
        
  <div class="container-fluid">
    <nav class="nav nav-tabs" style="border:none;">
    <ul class="nav navbar-nav navbar-right">
                   <!-- Authentication Links -->
                   @if (Auth::guest())
                       <li><a href="{{ url('/login') }}">Login</a></li>
                       <li><a href="{{ url('/register') }}">Register</a></li>
                    
                    @else
                    <li><a href="#">Hiba</a></li>
                    @endif
                    </ul>
                    </nav>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <!--  <img src="../public/logo.png" height="150px" width="150px"> -->
      
      <form >
      <a class="navbar-brand" style="font-family: Helvetica; font-size:25px; font-weight:bold; " href="#">Dorm Society | </a>
       <input  class="search" type="textarea" name="dorm_search" style="width:650px">    
       <button class="search" class="btn-default" class="form-control"  ><a href="{{ url('/home') }}">Search</a></button>
      </form>
       </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!--   <ul class="nav navbar-nav">
            <li ><a href="#">about us <span class="sr-only">(current)</span></a></li>
            <li><a href="#">see more</a></li>
        </ul>
        
        </div> -->
        </div>
    </nav>

           <!--  <li class="active"><a href="#">Home</a></li> -->
          
            <!-- <li><a href="{{ url('/addDorm') }}">I am dorm owner</a> -->
        
            <div class="container" id="container1">
            </div>
            <div class="container" id="container2"><h1>Find an Accomodation Now!</h1>
                
                  <div class="row">
                  <br>
                    <div class="col-lg-3 col-sm-3 ">
                     <form  method="POST" role="form"  enctype="multipart/form-data" action="{{ url('/post') }}">
                               {{ csrf_field() }}
                      <select class="form-control" name="Location">
                        <option>Location</option>
                        <option>Hamra, Beirut</option>
                        <option>Ashrafieh, Beirut</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                      <select class="form-control" name="Price">
                        <option>Price</option>
                        <option>$150 - $200</option>
                        <option>$200 - $250</option>
                        <option>$250 - $300</option>
                        <option>$300 - $400</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                    <select class="form-control" name="Time">
                        <option>Time</option>
                        <option>Fall 2016 </option>
                        <option>Spring 2017</option>
                        <option>Fall 2016</option>
                      </select>
                      </div>
                      <div class="col-lg-3 col-sm-4">
                      <button class="btn btn-success"  >
                      <!-- class="form-control" -->
                      <a href="{{ url('/home') }}">Find Now</a>
                      </button>
                      </div>
                      
                      </div>
                </div>
                </div>
                </div>
                </form>
                </div>

            </div>

            <div class="container" id="container3">

            <h2>
            Trending...</h2>
                <div class="row">
                      <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801320_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>Bliss Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> --><div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801337_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  Diva House </h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                          <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801303_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  City Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                           <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                </div>


            </div>
            <div class="container" id="container4">
                
            </div>
        </div>
        
    </body>



</html>
