<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
// use App\Http\Controllers\Auth;
// use App\Dorm;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $dorms = Dorm::orderBy('id', 'DESC')->get();
        // $users= User::all();

        return view('dormlist');
        // ->with('dorms', $dorms);
        // ->with('users',$users);
    }
    public function dorm()
    {
        return view('dorm');
    }

    public function tryo()
    {
        return view('tryo');
    }
    
    public function test()
    { 
        $dorms = Dorm::orderBy('id', 'DESC')->get();
        // $users= User::all();

        return view('test')->with('dorms', $dorms);
    }
    public function post(Request $request) 
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
        $image  = $request->file('image');
        // $user_id=$request->user()->id; 
        $input['imagename'] = time().'_'.$image->getClientOriginalExtension();
        $destinationPath = public_path('dorms_pictures');
        $image->move($destinationPath, $input['imagename']);
        //storing it in database
        $dorm = new Dorm();
        $dorm->dorm_name = $request->get('dorm_name');
        $dorm->dorm_description = $request->get('description');
        // $dorm->user_id = $request->user()->id;
        $dorm->dorm_image_path = $input['imagename'];
        $dorm->save();
        return redirect('/showDorm')->withMessage('Posted Successfully');
    }

    public function search(Request $request) 
    {
        
        //storing it in database
        $dorm_name=$request->get('dorm_name');

         $dorm = Dorm::where('dorm_name',$dorm_name)->get();
   if(isset($dorm)) {
           return redirect('/showDorm')->with('dorm', $dorms);
       }
       else {
           return null;
       }
       
       
    }

    public function showDorm()
    {
        $userId = Auth::id();
        $userName=User::find($userId)->name;
        $dorms = Dorm::orderBy('id', 'DESC')->get();
        return view('dorminfo')->with('dorm', $dorms[0])->with('username',$userName);
    }
   
}

