<?php
require_once("GameSolver.php");
require_once("GameGenerator.php");
$myGame=new GameGenerator();
$nGames=$myGame->__getNumberOfGames();
$game_num=1;
while($nGames+1>$game_num)
{
	$target=$myGame->__getTarget();
	$the_six_numbers =$myGame->__getSixRandomNumbers();
	echo "Game ".$game_num++."\n";
	$mySolution=new GameSolver();
	$mySolution->reachIt($target,$the_six_numbers);
}
?>
