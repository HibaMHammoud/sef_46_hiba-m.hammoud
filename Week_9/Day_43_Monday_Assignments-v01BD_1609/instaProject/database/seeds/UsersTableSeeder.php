<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => "Imad",
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ],[
            'name' => "Ola",
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ],[
            'name' => "Rabih",
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
