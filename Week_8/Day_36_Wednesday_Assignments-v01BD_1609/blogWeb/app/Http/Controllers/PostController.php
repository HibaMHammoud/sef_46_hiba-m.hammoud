<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\post;

class PostController extends Controller
{

	public function index()
    {
        $posts = post::all();
        return view('main')->with('posts', $posts);
    }

    public function postInfo($id)
    {
    	$post = post::find($id);
        $title =$post->title;
        $topic =$post->topic;
        $description =$post->description;
        $author= $post->author;
    	return view('info')->with(['title' => $title ,'topic' => $topic ,'description' => $description, 'author' => $author ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    	$post = new post;
	    $post->title = $request->get('title');
	    $post->topic =  $request->get('topic');
	    $post->description =  $request->get('description');
	    $post->author = $request->get('author') ;
	    // save the post to the database
	    $post->save();
	    return redirect('/index')->withMessage('Posted Successfully');
    }
}
