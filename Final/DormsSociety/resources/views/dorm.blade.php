@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dorm Society</title>

        <style>
            html, body {
                background-color: #fff;
                color: white;
                font-family: 'Helvetica';
                font-weight: 100;
                height: 100vh;
                margin: 0;
                 background: black;
            }




             .container{
                max-height: 1000px;
                width: 100%;
                border: 1px solid #e7e7e7;
                border-radius: 4px;
                  background: white;     
            }
            img{
                height: 500px;
                width: 100%;
            }
            nav{
                background-color: #000066;
                color: white;
            }

            .navbar-brand{
                color:#000066;
            }
            .black{
                background-color: black;
            }
        </style>
    </head>
    <body>
    <div class="container-fluid">
        
        <nav class="nav nav-tabs" style="border:none;">
            <ul class="nav navbar-nav navbar-right">
               <!-- Authentication Links -->
               @if (Auth::guest())
               <li><a href="{{ url('/login') }}">Login</a></li>
               <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                <li><a href="#">Hiba Hammoud</a></li>
                @endif
                </nav>
            </ul>
        </nav>    

        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" style="font-family: Helvetica; font-size:25px; font-weight:bold; " href="#">Dorm Society </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            </div>
            </div>
            </div>

            <div class="black">
            <br>
            <br>
            <div class="container">
            <h2>Diva House</h2>
            
                <img src="../public/dorms_pictures/1476801337_jpg" alt="dorm_name" >
               <!--  <div class="row">
                    <div class="col-sm-2 col-sm-offset-1">Overview</div>
                          <div class="col-sm-2">Services</div>
                          <div class="col-sm-2">Location</div>
                          <div class="col-sm-2">Reviews</div>
                          <div class="col-sm-2">Rules</div>
                </div> -->
                <ul class="nav nav-tabs">
                  
                  <li role="presentation" class="active"><a href="#">Overview</a></li>
                  <li role="presentation"><a href="#">Services</a></li>
                  <li role="presentation"><a href="#">Location</a></li>
                  <li role="presentation"><a href="#">Reviews</a></li>
                  <li role="presentation" ><a href="#">Rules</a></li>
                   <li role="presentation" ><a href="#">Contact Info</a></li>
                </ul>

                <h3>    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi venenatis gravida ante nec vehicula. Ut non tincidunt sem. Nullam tempus erat felis, ac euismod augue commodo in. Praesent sit amet lacus quis sapien condimentum malesuada ac eu lorem. Nunc sagittis non ipsum eget eleifend. Nam ornare magna vitae lacinia pulvinar. Donec a luctus sapien. In hac habitasse platea dictumst. Praesent sit amet dignissim orci. Nullam ut facilisis nunc, vel tincidunt ligula. Nam rutrum sem est, non scele</h3>
            </div>
            </div>
            </div> 
            <br>
            <br>        
         
        </div>
        <div class="container">
        Email:divahouse@test.com &nbsp &nbsp Phone:01/6654044 ext:90<br>
        Location: Makhoul Street, Hamra,Beirut, Lebanon;<br>
        </div>
    </body>
</html>    
