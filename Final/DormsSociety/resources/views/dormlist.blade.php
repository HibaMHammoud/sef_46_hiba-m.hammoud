@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <style>
            html, body {
                background-color:rgba(171, 25, 19, 0.94); 
                color: white;
                font-family: 'Helvetica';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .container{
                height: 400px;
                width: 100%;
                
            }
            #container1{
               
                background-image: url("../public/images.jpg");
                background-size: 100% 100%;
                background-color: #210B61;
            }
            #container2{
                height:75px;
               
                background-color: black;
            }
            #container3{

              height: 400px;
                /*background-color: white; */
                 background-color:rgba(171, 25, 19, 0.94); 
            }
            #container4{
               background-color: white;
            }
            nav{
                background-color: #000066;
                color: white;
            }

            .navbar-brand{
                color:#000066;
            }
        </style>
        <title>Dorm Society</title>
</head>
<body>

<div class="container-fluid">
    <nav class="nav nav-tabs" style="border:none;">
    <ul class="nav navbar-nav navbar-right">
                   <!-- Authentication Links -->
                   @if (Auth::guest())
                       <li><a href="{{ url('/login') }}">Login</a></li>
                       <li><a href="{{ url('/register') }}">Register</a></li>
                    
                    @else
                    <li><a href="#">Hiba</a></li>
                    @endif
                    </ul>
                    </nav>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <!--  <img src="../public/logo.png" height="150px" width="150px"> -->
      <a class="navbar-brand" style="font-family: Helvetica; font-size:25px; font-weight:bold; " href="#">Dorm Society | </a> </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!--   <ul class="nav navbar-nav">
            <li ><a href="#">about us <span class="sr-only">(current)</span></a></li>
            <li><a href="#">see more</a></li>
        </ul>
        
        </div> -->
        </div>
    </nav>

            <!-- <li class="active"><a href="#">Search</a></li> -->
          
            <!-- <li><a href="{{ url('/addDorm') }}">I am dorm owner</a> -->
        
            <!-- <div class="container" id="container1">
            </div> -->
            <div class="container" id="container2">
                <br>

                  <div class="row">
                  <div class="col-lg-3 col-sm-3 ">
                  <form  method="POST" role="form"  enctype="multipart/form-data" action="{{ url('/post') }}">
                               {{ csrf_field() }}
                      <select class="form-control" name="Location">
                        <option>Hamra, Beirut</option>
                        <option>Hamra, Beirut</option>
                        <option>Ashrafieh, Beirut</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                      <select class="form-control" name="Price">
                        <option>$150 - $200</option>
                        <option>$150 - $200</option>
                        <option>$200 - $250</option>
                        <option>$250 - $300</option>
                        <option>$300 - $400</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                    <select class="form-control" name="Time">
                        <option>Spring 2017</option>
                        <option>Fall 2016 </option>
                        <option>Spring 2017</option>
                        <option>Fall 2016</option>
                      </select>
                      </div>
                      <div class="col-lg-3 col-sm-4">
                      <button class="btn btn-success" class="form-control" >Search</button>
                      </div>
                </div>
                </div>
                </div>
                </form>
                  </div>   
                </div>

            </div>

            <div class="container" id="container3">

            <h2>
            Voila...</h2>
                <div class="row">
                      <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801320_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>Bliss Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> --><div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                      </div>
                                </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801337_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  Diva House </h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                          <div class="row">
                                    <div class="col-md-offset-8 col-md-4">

                                           <a href="{{ url('dorm') }}" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801303_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  City Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                           <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
               <div class="row">
                      <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801320_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>Bliss Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> --><div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                      </div>
                                </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801337_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  Diva House </h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                          <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801303_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  City Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                           <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                </div>
            </div>

               <div class="row">
                      <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801320_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>Bliss Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> --><div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                      </div>
                                </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801337_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  Diva House </h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                          <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                                <div class="col-md-4">
                        <div class="thumbnail" style="height: 300px;">
                          <img src="../public/dorms_pictures/1476801303_jpg" alt="dorm_name" style="height: 175px;" >
                          <div class="caption">
                            <h3>  City Dorm</h3>
                           <!--  <p></p> -->
                            <p>
                            <!-- <a href="#" class="btn btn-primary" role="button">Button</a> -->
                                           <div class="row">
                                    <div class="col-md-offset-8 col-md-4">
                                           <a href="#" class="btn btn-default"  role="button">See more</a>
                                        </div>
                                        </div>
                            </p>
                          </div>
                        </div>
                      </div>
                </div>
            </div>

           <!--  <div class="container" id="container4">
                
            </div> -->
        </div>
  
</body>
</html>
