<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
// Route::get('home/{id}', 'HomeController@info');
Route::get('/dorm', 'HomeController@dorm');

Route::get('/tryo', 'HomeController@tryo');

 // Route::get('/dorms', 'HomeController@generalSearch');
 Route::group(['middleware' => ['auth']], function()
{
    Route::get('/addDorm', 'HomeController@create');
    Route::post('/post', 'HomeController@post');
    Route::post('/search', 'HomeController@search');
    Route::get('/showDorm', 'HomeController@showDorm');
    Route::get('/test', 'HomeController@test'); 
});

// Route::get('/home/dorms', 'HomeController@filterSearch');
// Route::get('/home/dorms/{dorm_id}', 'HomeController@info');
// Route::get('/home/dorms/{dorm_id}/description', 'HomeController@description');
// Route::get('/home/dorms/{dorm_id}/rules', 'HomeController@rules');
// Route::get('/home/dorms/{dorm_id}/pictures', 'HomeController@showPictures');
// Route::get('/home/dorms/{dorm_id}/pictures/{picture_id}', 'HomeController@showPicture');
// Route::get('/home/dorms/{dorm_id}/apply', 'HomeController@apply');
