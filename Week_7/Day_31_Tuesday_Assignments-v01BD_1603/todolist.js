function getLastKey()
{
		var count=localStorage.getItem('Counter');
		return count;
}

function displayInfoInHtml(task_title , task_description ,time )
{
	//Creating a Div task element with ID (= key in local storage) + with class task-list-element
	var task_element = document.createElement("DIV");
	task_element.setAttribute("class","task-list-element");
	task_element.setAttribute("id",getLastKey());
	var task_text = document.createTextNode(task_title+": "+ task_description);
	var time_text = document.createTextNode(time);
	//creating a remove button X with remove function + adding it as a child of div
	var br_element=document.createElement("BR");
	task_element.appendChild(task_text);
	task_element.appendChild(br_element);
	task_element.appendChild(time_text);
	var remove_button=document.createElement("BUTTON");
	remove_button.setAttribute("class","remove-button");
	remove_button.setAttribute("onclick","removeTask(this)");
	var remove_sign_text = document.createTextNode("X");
	remove_button.appendChild(remove_sign_text);
	task_element.appendChild(remove_button);
	//adding the last added elemet=nt of the list first     
	var task_list=document.getElementById("task-list");
	task_list.insertBefore(task_element, task_list.childNodes[0]);
	
}

function incrementCounterAndReturnLastKey()
{
	var last_key = "";
	var count = localStorage.getItem('Counter');
	if(count === null){
	  	last_key+= 1;
	localStorage.setItem('Counter',last_key);
	 	return last_key;
	} else {
		var new_count = parseInt(count)+1;
		last_key+= new_count;
		localStorage.setItem('Counter',last_key );
		return last_key;
	}
}

function addTask()
{
	var task_title = document.getElementById("task-title").value;	
	var task_description = document.getElementById("task-description").value;
	var date_now=Date(); 
	var time = new Date(date_now).toLocaleString("en-US",  { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' , hour: '2-digit', minute:'2-digit' });
	var task_info = task_title + " " + task_description +" "+time ;
		//Checking the conditions
	if(task_title.length < 1 || task_title.length > 40){
		alert("Please add a valid title");
		return null;
	}
	if(task_description.length <1){
		alert("Please add a description");
		return null;
	}
		//Adding Item to storage
 	var key=incrementCounterAndReturnLastKey();
	localStorage.setItem(key, task_info);
	//Creating child element in the task list
	displayInfoInHtml(task_title , task_description ,time );  
	//emptying the textareas after adding theelement
	document.getElementById("task-title").value="";
	document.getElementById("task-description").value="";
}

function removeTask(button_element)
{
	// removing the Element from HTML page
	var task_element=button_element.parentElement;
	var task_element_id=task_element.id;
	parent=document.getElementById("task-list");
	parent.removeChild(task_element);
	alert("Deleted");
}

function checkHistory()
{
	var history="";
	for(var i in localStorage) {
	   history+=  i+ " "+localStorage.getItem(i)+"\n";
	}
	alert(history);
}

function clearHistory()
{
	localStorage.clear();
	alert("History Cleared");
	document.getElementById("task-list").innerHTML="";
}