<html>
   <head>
      <title>Create a  blog</title>
      <link href={{ url('/css/create.css') }} rel="stylesheet">
   </head>
   <body>
         
        <h1>Create a post</h1>

        <form role="form" action="{{ url('/create') }}" method="POST">
                           {{ csrf_field() }}
            <input type = "hidden" name = "_token" value="{{ csrf_token() }}" />
                <p>
                    <label for="title">Title:</label>
                    <input type = "text" name = "title" /><br>
                </p>
                <p>
                    <label for="topic">Topic</label>
                    <input type = "text" name = "topic" /><br>
                </p>
                <p>
                    <label for="description">Description</label>
                    <textarea  name = "description" rows='15' cols="125"></textarea><br>
                </p>
                <p>
                    <label for="name">Author</label>
                    <input type = "text" name = "author" /></p>
                <p>
                    <input id ="post-button" type = "submit" value = "Post" />
                </p>
        </form>
   </body>
</html>