@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
      <title>My profile</title>
      <style>
    	body { 
      	padding-bottom: 70px;
      	}
      	section{
      		border-style: solid;
		    border-color: black;
		   /* #032C52;*/
		    border-style: bold;
		    border-radius: 30px;
		    padding: 30px; 
		   /* //background-color: #A4D4FF;*/
		    border-width: 15px;
      	}
      	#top-bar{
         padding-top: 10px;  
        }
      	.vertical-center {
			min-height: 100%;
			min-height: 100vh;
			display: flex;
			align-items: center;
		}
      </style>
</head>
	  <nav class="navbar navbar-default navbar-fixed-top" id='top-bar'>
          <!--   <a href="{{ url('/showProfile') }}">My profile</a>
                <a href="{{ url('/create') }}">Add a dorm</a>  -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">dORM sOCIETY</a>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <!-- <li><a href="{{ url('/showDorm') }}">My ProfilE
                  </a></li> -->
                  <li><a href="{{ url('/addDorm') }}">add a dorm</a></li>
                </ul>
            </div>
        </nav>
 <div class="jumbotron vertical-center">
            <div class="container-fluid">
                <div class="page-header">
                  <h1> {{ $dorm->dorm_name }}<small>Dorm Society</small></h1>
                </div>
               
                <section>
                  
                    <label><img  class="img-rounded" src="{!! url('/dorms_pictures/' . $dorm->dorm_image_path) !!}"  width="400" height="400" > </label> <br>
                     
                    <label>{{ $dorm->dorm_name }}</label><br>
                    <label>{{ $dorm->dorm_description }}</label><br>
                </section>      
            </div>
            <!-- <nav class="navbar navbar-default navbar-fixed-bottom" id= "bottom-bar">
                <div class="container"></div>
            </nav> -->
        </div>
    </body>
</html>
