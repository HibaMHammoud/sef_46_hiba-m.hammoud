@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
      <title>Home</title>
      <style>
        body{ 
        padding-bottom: 70px;
        }
        #top-bar{
         padding-top: 10px;  
         }
        section{
            border-style: solid;
            border-color: black;
           /* #032C52;*/
            border-style: bold;
            border-radius: 30px;
            padding: 30px; 
           /* //background-color: #A4D4FF;*/
            border-width: 15px;
        }
        span{
            /*color: red;*/
            border:none;
        }
        .vertical-center {
            min-height: 100%;
            min-height: 100vh;
            display: flex;
            align-items: center;
        }
      </style>
</head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" id='top-bar'>
            <!-- <a href="{{ url('/showProfile') }}">My profile</a>
                <a href="{{ url('/create') }}">Add a post</a>  -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Instagram</a>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="{{ url('/showProfile') }}">My Profile</a></li>
                  <li><a href="{{ url('/create') }}">Create a post</a></li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron vertical-center">
            <div class="container-fluid">
                <div class="page-header">
                  <h1>Home <small>Instagram</small></h1>
                </div>
                @foreach ($posts as $post)
                <section>
                    <label>{{ $users->find($post->user_id)->name }} </label><br>
                    <label><img  class="img-rounded" src="{!! url('/users_pictures/' . $post->post_image_path) !!}"  width="400" height="400" > </label> <br>
                     <button type="button" class="btn btn-danger btn-lg pull-right" aria-label="Left Align">
                    <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                    </button><br>
                    <label>{{ $post->post_title }}</label><br>
                    <label>{{ $post->post_description }}</label><br>
                </section>      
                @endforeach 
            </div>
            <nav class="navbar navbar-default navbar-fixed-bottom" id= "bottom-bar">
                <div class="container"></div>
            </nav>
        </div>
    </body>
</html>
