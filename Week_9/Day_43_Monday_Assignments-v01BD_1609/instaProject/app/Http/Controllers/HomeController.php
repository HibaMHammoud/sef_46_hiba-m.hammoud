<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use App\Like;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->get();
        $users= User::all();

        return view('home')->with('posts', $posts)->with('users',$users);
    }
    public function create()
    {
        return view('createPost');
    }
    public function post(Request $request) 
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
        $image  = $request->file('image');
        $user_id=$request->user()->id; 
        $input['imagename'] = time().'_'.$user_id.'_'.$image->getClientOriginalExtension();
        $destinationPath = public_path('users_pictures');
        $image->move($destinationPath, $input['imagename']);
        //storing it in database
        $post = new Post();
        $post->post_title = $request->get('title');
        $post->post_description = $request->get('description');
        $post->user_id = $request->user()->id;
        $post->post_image_path = $input['imagename'];
        $post->save();
        return redirect('/showProfile')->withMessage('Posted Successfully');
    }
    public function showProfile()
    {
        $userId = Auth::id();
        $userName=User::find($userId)->name;
        $posts = Post::orderBy('id', 'DESC')->where('user_id',$userId)->get();
        return view('profile')->with('posts', $posts)->with('username',$userName);
    }
}
