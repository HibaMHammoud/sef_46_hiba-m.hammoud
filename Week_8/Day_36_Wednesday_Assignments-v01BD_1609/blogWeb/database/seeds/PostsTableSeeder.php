<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  //'id','title','topic','description','author');
         Eloquent::unguard();

    	DB::table('posts')->insert(array(
    		array('title' => 'mytitle1', 'topic' => 'anglais','description' => 'project-1','author' => 'Hiba' ),
    		array('title' => 'mytitle2', 'topic' => 'anglais','description' => 'project-2','author' => 'LouLou' ),
    		array('title' => 'mytitle2', 'topic' => 'anglais','description' => 'Laravel s database query builder provides a convenient, fluent interface to ...... The query builder may also be used to delete records from the table via the delete ...
‎Introduction · ‎Selects · ‎Joins · ‎Advanced Wheres
You visited this page on 9/7/16.','author' => 'Bia' ),
    		array('title' => 'mytitle', 'topic' => 'anglais','description' => 'project-3','author' => 'Roro' )
    		));
        //
    }
}
