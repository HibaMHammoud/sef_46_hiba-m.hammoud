<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Eloquent::unguard();
    	DB::table('posts')->insert(array(
    		array('post_title' => 'I am title 1', 'post_description' => 'fjnfkewjn','post_image_path' => 'a.jpg','user_id' => '1' ),
    		array('post_title' => 'mypost_title2', 'post_description' => 'anglais','post_image_path' => 'b','user_id' => '2' ),
    		array('post_title' => 'mypost_title2', 'post_description' => 'anglais','post_image_path' => 'c','user_id' => '3' ),
    		array('post_title' => 'mytitle', 'post_description' => 'anglais','post_image_path' => 'f.jpg','user_id' => '2' )
    		));
    }
}
