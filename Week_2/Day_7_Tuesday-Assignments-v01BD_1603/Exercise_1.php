<?php
echo "write stop to stop \n";
$line = "";
$assigned_color = array();
while($line != "stop")
{
	$line = readline();
	if($line == "Black" || $line == "White")
	{
	array_push($assigned_color, $line);
	}
}

$reversed_array = array_reverse($assigned_color);
$n = count($assigned_color);//number of people in the group
$reversed_guesses = array($n);
//black is the base color
for($i = 0 ; $i < $n; $i++)
{
        array_shift($reversed_array);
	$reversed_guesses[$i] = canGuess($i,$reversed_array, $reversed_guesses);
	echo  $reversed_guesses[$i]."\n";
}

//results
$guesses=array_reverse($reversed_guesses);
$mistakes = checkingAnswers($assigned_color, $guesses);
if($mistakes < 1) echo "Congratulations, the group  passed the challenge";
else echo "The Group lost the  challenge";

function checkingAnswers($original,$guesses)
{
$mistake_counter = 0;
	foreach ($original as $color)
	{	
	$guess = array_shift($guesses);
		if($color != $guess)
		{
		$mistake_counter++;
		}
	}
return $mistake_counter;
}

function canGuess($index, $front_hats, $back_hats)
{
	$front_blackhats = canSeeBlack($index, $front_hats);
	$back_blackhats = canHearBlack($index, $back_hats);
	if($index == 0)
	{
		if($front_blackhats % 2 == 0)//if the front black hats are even the first person sends a message white
		{
			return "White";
		}
		else return "Black";
	}
	else if($back_hats[0] == "Black")//odd black hats from the message given by the last person
	{
		if(($front_blackhats + $back_blackhats) % 2 == 0) return "Black";
		else return "White";
	}
	else if($back_hats[0] == "White")//even black hats
	{
		if(($front_blackhats + $back_blackhats) % 2 == 0) return "White";
		else return "Black";
	}

}

function canSeeBlack($index, $front_hats)
{
$blackseen = 0;
	foreach ($front_hats as $color)
	{
		if($color == "Black")
		{
			$blackseen++;
		}
	}
return $blackseen;
}

function canHearBlack($index, $guesses)
{
$blackheard = 0;
$guesses[0] = "Blank";
	foreach ($guesses as $color)
	{
		if($color == "Black")
		{
			$blackheard++;
		}
	}
return $blackheard;
}
?>

